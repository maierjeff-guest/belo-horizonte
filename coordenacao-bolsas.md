# Coordenação das bolsas

A ideia é listar aqui pontos que a pessoa que for coordenar o processo de pedido
de bolsas na MiniDebConf Belo Horizonte possa trabalhar.

## Antes

Temos que tomar algumas decisões prévias antes mesmo de abrirmos a inscrição os
participantes.

 * Qual será o valor total para passagens que vamos pedir ao Debian?
 * Após a abertura das inscrições, qual será o prazo máximo para pedir a bolsa?
 * Definir a data da divulgalçao do resultado.
 * Definir o prazo para avaliação.
 * Trabalhar com o Terceiro no formulário de pedido de bolsa. Tem itens para modificar, tirar ou adicionar? 
 * Ler a [página de bolsas](https://bh.mini.debconf.org/evento/bolsas/) e decidir sobre alguns pontos estão lá:
    * Teremos uma bolsa geral, ou vamos separar para diversidade?
    * Se tiver bolsa de diversidade, quais grupos são prioridade? No site tem 3 listados em "Nós especialmente gostaríamos de receber pedidos de".
    * O pagamento será via SPI (internacional) ou via ICTL (Brasil)? Isso dependerá se o ICTL quer assumir isso, e se o Líder permitirá usar o ICTL.
    * Despesas de uber/taxi serão cobertas? e Alimentação nos deslocamentos?

## Durante

Enquanto estiver rolando as inscrições, é preciso formar um grupo (quantas
pessoas?) que irá avaliar os pedidos e pontuar de acordo com os critérios
estabelecidos.

Vai ser necessário entender como funciona o sistema de dar permissão de
avaliador (referees) para as pessoas do grupo.

## Depois

Após o encerramento das inscrições das bolsas, começar o processo de avaliação.

 * Alguém deve ser rejeitado logo para não precisar passar pela avaliação?
 * Alguém deve ser aceito logo para não precisar passar pela avaliação? Por exemplo, organizadores, DDs brasileiros, outros?
 * Dependendo da quantidade de pedidos, podemos aceitar logo os pedidos de hospedagem e alimentação para quem não está pedindo passagens?
 * Montar o ranking após a avaliação.
 * Começar a divulgar quem ganhou as bolsas, pedir confirmação dessas pessoas, etc.
 
Perguntar para o Stefano se essas orientações ainda está valendo, principalmente
a parte dos scripts <https://salsa.debian.org/debconf-bursaries-team/bursaries/dc22/-/blob/main/ranking-data/README.md>

## Importante

Por favor não deixe para conhecer o sistema de bolsas após o prazo de pedido
porque isso atrasará bastante o processo.

Acredito que dá pra conversar com o Stefano e o Terceiro pra ver se eles
permitem o acesso ao sistema de alguma DebConf anterior, pra entender na
prática como o sistema funciona.

