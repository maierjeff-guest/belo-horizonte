account assets:SPI
account assets:debian-ch
account assets:debian-france
account assets:stripe
account assets:cash
account assets:ICTL

account expenses:bursaries:general bursaries
account expenses:bursaries:diversity bursaries
account expenses:bursaries:travel
account expenses:transport
account expenses:transport:general
account expenses:transport:attendees
account expenses:transport:daytrip
account expenses:ICTL fees
account expenses:general
account expenses:graphic materials
account expenses:graphic materials:banner
account expenses:graphic materials:paper
account expenses:graphic materials:poster
account expenses:graphic materials:general
account expenses:graphic materials:badge
account expenses:graphic materials:sticker
account expenses:incidentals
account expenses:incidentals:bank
account expenses:incidentals:frontdesk
account expenses:incidentals:misc
account expenses:incidentals:transport
account expenses:infra
account expenses:infra:rent
account expenses:insurance
account expenses:local team:general
account expenses:local team:reimbursements
account expenses:local team:transport
account expenses:local team:fuel
account expenses:postal
account expenses:press
account expenses:accommodation:bedrooms
account expenses:accommodation:room
account expenses:accommodation:coffee and water
account expenses:food:lunch
account expenses:food:dinner
account expenses:food:coffee break
account expenses:food:daytrip
account expenses:food:coffee and tea
account expenses:sponsors
account expenses:swag:backpack
account expenses:swag:badge paper
account expenses:swag:drink cup
account expenses:swag:lanyard
account expenses:swag:tag
account expenses:swag:t-shirt
account expenses:swag:badge-bag
account expenses:tax
account expenses:travel costs for invited speaker
account expenses:venue:rent
account expenses:venue:staff
account expenses:video:cable
account expenses:video:computer rental
account expenses:video:computer
account expenses:video:fiber
account expenses:video:general
account expenses:video:insurence
account expenses:video:projector
account expenses:video:sound equipment
account expenses:video:camcorder rental
account expenses:visa
account incomes:registration
account incomes:registration:paypal for SPI
account incomes:registration:cash for ICTL
account incomes:daytrip
account incomes:daytrip:cash for ICTL
account expenses:daytrip:transport
account expenses:daytrip:food

account incomes:general
account incomes:general:cash for ICTL
account incomes:accommodation
account incomes:bar
account incomes:bar:cash for ICTL
account incomes:bar:credit card for ICTL
account incomes:SPI reimbursement received
account incomes:SPI reimbursement to receive
account incomes:sponsors:national:bronze
account incomes:sponsors:international:bronze
account incomes:sponsors:national:gold
account incomes:sponsors:international:gold
account incomes:sponsors:national:platinum
account incomes:sponsors:international:platinum
account incomes:sponsors:national:silver
account incomes:sponsors:international:silver
account incomes:sponsors:national:supporter
account incomes:sponsors:international:supporter
account incomes:sponsors:national:donations
account incomes:sponsors:international:donations

; Create liabilility accounts for orga incurring expenses here

account liabilities:phls

