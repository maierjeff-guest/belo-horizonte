# Hoteis



Hotel | Duplo | Disponíveis | Auditório | Buffet | Distância
----- | ----- | ----------- | --------- | ------ | ---------
**Hotel Stop Inn** | 243,00 | ? | 420,00  | 58,00 | 1,7
Sossego da Pampulha | 390,00 | ? | Não | - | 1,7
Hotel Minas Pampulha | - | ? | Não | - | 1,4
**Hotel BH Jaraguá** | 321,00 | ? | 630,00 | 61,00 | 1,6
Hotel Allia | 306,00 | ? | 790,00 | 52,00 | 2,1
Pampulha Flat | 255,00 | ? | Não | - | 1,8
Hotel Quality | 346,00 | ? | Sim | - | 1,9
Hotel Nobile | Esperando resposta | ? | ? | - | 2,1
Hotel Pampulha Lieu | - | ? | Sim | - | 2,2

## Av. Antonio Abrahão Caram

Hotel Stop Inn Plus Pampulha

 * <https://www.hotelstopinnpampulha.com.br>
 * 26 min - 1,7km
 * Auditório
 * mandei email 03/11
 * Duplo: R$ 242,55

Pousada Sossego da Pampulha

 * <https://sossegodapampulha.com.br>
 * 26 min - 1,6km
 * mandei email 04/11
 * Duplos: R$ 390,00 (só 5 quartos)
 * Triplo: R$ 500,00 (só 4 quartos)

Hotel Minas Pampulha

 * 24 min - 1,4km

## Av Pres Antonio Carlos (em frente)

BH Jaraguá Hotel

 * <https://www.bhjaraguahotel.com.br>
 * 23 min - 1,6km
 * Auditório
 * mandei email 03/11
 * Duplo: R$ 320,25

Allia Gran Pampulha Suítes

 * <https://www.alliagranpampulhasuites.com.br>
 * 32 min - 2,1km
 * Auditório
 * mandei email 03/11

## Av Pres Antonio Carlos (depois viaduto)

Pampulha Flat

 * <https://www.pampulhaflat.com>
 * 24 min - 1,8km
 * mandei email 03/11
 * Duplo: R$ 255,00

Quality Hotel Pampulha

 * <https://www.reserveatlantica.com.br/hotel/quality-hotel-pampulha-convention-center>
 * 26 min - 1,9km
 * Auditório
 * mandei email 03/11
 * Duplo: R$ 345,45

Nobile Inn Pampulha

 * <https://www.nobilehoteis.com.br/sudeste/minas-gerais/belo-horizonte/nobile-inn-pampulha>
 * 28 min - 2,1km
 * mandei email 03/11


## Av Pres Carlos Luz

Pampulha Lieu Hotel
 * Auditório
 * 34 min - 2,2km
