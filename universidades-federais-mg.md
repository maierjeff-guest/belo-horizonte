# Universidades Federal em Minas Gerais

UFMG

 * https://dcc.ufmg.br

UFJF - Universidade Federal de Juiz de Fora

 * https://www2.ufjf.br/engcomputacional
 * https://www.ufjf.br/cursocomputacao
 * https://www.ufjf.br/si/

UFU - Universidade Federal de Uberlândia

 * https://facom.ufu.br/graduacao/ciencia-da-computacao
 * https://facom.ufu.br/graduacao/sistemas-de-informacao-campus-santa-monica
 * https://facom.ufu.br/graduacao/sistemas-de-informacao-campus-monte-carmelo

UFV - Universidade Federal de Viçosa

 * https://ccp.ufv.br/

UFSJ - Universidade Federal de São João del-Rei

 * https://ccomp.ufsj.edu.br/

UFOP - Universidade Federal de Ouro Preto

 * https://www.escolha.ufop.br/cursos/ciencia-da-computacao
 * https://www.escolha.ufop.br/cursos/engenharia-de-computacao
 * https://www.escolha.ufop.br/cursos/sistemas-de-informacao

UFLA - Universidade Federal de Lavras

 * https://icet.ufla.br/graduacao/ciencia-computacao-bacharelado
 * https://icet.ufla.br/graduacao/sistemas-informacao-bacharelado

<https://pt.wikipedia.org/wiki/Lista_de_institui%C3%A7%C3%B5es_de_ensino_superior_de_Minas_Gerais>