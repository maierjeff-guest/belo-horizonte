# Locais com auditórios em BH

<https://venueful.com/br/brasil/melhores-locais-para-eventos-de-negocios-em-belo-horizonte>

 * UFMG
 * CEFET-UFMG
    * Campus 2 - Nova Gameleira
        * 2 auditórios: um no prédio principal, um no prédio 20 (DECOMP)
 * [Centro de Referência das Juventudes](https://prefeitura.pbh.gov.br/smasac/sudc/equipamentos/crj)
 * [Centro de Convenções da Associação Médica](https://cenconammg.com.br)
 * [Fecomércio](https://www.fecomercio-sc.com.br/produtos/auditorio-fecomercio)
 * [SESI Minas](https://www.fiemg.com.br/sesi-cultura/seu-evento-aqui/detalhe/auditorio-sesiminas)
 * Sesc Tupinambas (180 lugares - sem equipamentos)
 * Expominas BH
 * Minascentro
 * Sebrae
 * BH-TEC
